package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"myapp/graph/generated"
	"myapp/graph/model"
	"myapp/service"
)

func (r *todoOpsResolver) Create(ctx context.Context, obj *model.TodoOps, newTodo string) (*model.Todo, error) {
	return service.TodoCreate(ctx, newTodo)
}

func (r *todoOpsResolver) UpdateStatus(ctx context.Context, obj *model.TodoOps, id int, isDone bool) (*model.Todo, error) {
	return service.TodoStatusUpdate(ctx, id, isDone)
}

func (r *todoOpsResolver) Delete(ctx context.Context, obj *model.TodoOps, id int) (*int, error) {
	return service.TodoDelete(ctx, id), nil
}

// TodoOps returns generated.TodoOpsResolver implementation.
func (r *Resolver) TodoOps() generated.TodoOpsResolver { return &todoOpsResolver{r} }

type todoOpsResolver struct{ *Resolver }
